module.exports = function (grunt) {

  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    clean: {
      build: ["test/**", "lib/**", 'app/**/*.dest.js']
      //release: ["path/to/another/dir/one", "path/to/another/dir/two"]
    },
    jshint: {
      files: ['Gruntfile.js', 'lib/**/*.js', 'test/**/*.js'],
      options: {
        // options here to override JSHint defaults
        globals: {
          jQuery: true,
          console: true,
          module: true,
          document: true
        }
      }
    },
    uglify: {
      dynamic_mappings: {
        // Grunt will search for "**/*.js" under "lib/" when the "uglify" task
        // runs and build the appropriate src-dest file mappings then, so you
        // don't need to update the Gruntfile when files are added or removed.
        files: [{
          expand: true,     // Enable dynamic expansion.
          flatten: true,     // Enable dynamic expansion.
          cwd: 'build/',      // Src matches are relative to this path.
          src: ['**/*.js'], // Actual pattern(s) to match.
          dest: 'lib/',   // Destination path prefix.
          ext: '.min.js',   // Dest filepaths will have this extension.
          extDot: 'last'   // Extensions in filenames begin after the first dot
        }]
      }
    },
    mochaTest: {
      test: {
        options: {
          ui: 'bdd',
          reporter: 'spec',
          //captureFile: 'results.txt', // Optionally capture the reporter output to a file
          quiet: false, // Optionally suppress output to standard out (defaults to false)
          clearRequireCache: false // Optionally clear the require cache before running tests (defaults to false)
        },
        src: ['test/**/*.mocha.js']
      }
    },
    coffee: {
      dynamic_mappings: {
        // Grunt will search for "**/*.js" under "lib/" when the "uglify" task
        // runs and build the appropriate src-dest file mappings then, so you
        // don't need to update the Gruntfile when files are added or removed.
        files:
          ['app']
          .map(
            function (item) {
              return [{
                expand: true,     // Enable dynamic expansion.
                cwd: item,      // Src matches are relative to this path.
                src: ['**/*.coffee'], // Actual pattern(s) to match.
                dest: item + '/',   // Destination path prefix.
                ext: '.dest.js',   // Dest filepaths will have this extension.
                extDot: 'last'   // Extensions in filenames begin after the first dot
              }, {
                expand: true,     // Enable dynamic expansion.
                flatten: true,     // Enable dynamic expansion.
                cwd: item + '/tests',      // Src matches are relative to this path.
                src: ['**/*.coffee'], // Actual pattern(s) to match.
                dest: 'test',   // Destination path prefix.
                ext: '.mocha.js',   // Dest filepaths will have this extension.
                extDot: 'last'   // Extensions in filenames begin after the first dot
              }];
            }).reduce(function (pairPrev, pairNext) { return pairPrev.concat(pairNext); })
      }
    },
    exec: {
      node: 'node ./build/app/app.js',
      nodecoffee: 'coffee app/app.coffee'
    }
  });

  grunt.loadNpmTasks('grunt-contrib-coffee');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-clean');
  grunt.loadNpmTasks('grunt-mocha-test');
  grunt.loadNpmTasks('grunt-exec');
	
  grunt.registerTask('dev', ['clean', 'coffee', 'uglify', 'mochaTest', 'exec:node']);
  grunt.registerTask('default', ['clean', 'coffee']);

};