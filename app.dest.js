(function() {
  var app, bodyParser, express, hamlc, index, lecturers, multer, partials, path, pathify, port, upload;

  express = require('express');

  app = express();

  bodyParser = require('body-parser');

  partials = require('express-partials');

  multer = require('multer');

  hamlc = require('haml-coffee').__express;

  path = require('path');

  pathify = function(p) {
    return path.join(__dirname, p);
  };

  app.use(express["static"](pathify("public")));

  app.use(bodyParser.json());

  app.use(bodyParser.urlencoded({
    extended: true
  }));

  upload = multer({
    dest: pathify('uploads')
  });

  partials.register('.hamlc', function(src, opts) {
    return hamlc(opts.filename, opts, function(err, result) {
      if (err) {
        throw err;
      }
      return result;
    });
  });

  app.engine('hamlc', hamlc);

  app.set('view engine', 'hamlc');

  app.set('layout', 'layout');

  app.set("views", pathify("views"));

  app.enable("view cache");

  app.use(partials());

  index = require(pathify("routes/index.route"));

  index(app);

  lecturers = require(pathify("routes/lecturers.route"));

  lecturers(app);

  port = parseInt(process.env.PORT || 3000, 10);

  app.listen(port, function() {
    return console.log("Okul-Otomasyon " + port + " Port'undan Dinlemede!");
  });

}).call(this);
