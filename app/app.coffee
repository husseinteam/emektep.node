express 			= require 'express'
app 				= express()
bodyParser			= require 'body-parser'
partials			= require 'express-partials'
multer				= require 'multer'
hamlc 				= require('haml-coffee').__express
path				= require 'path'

pathify = (p) -> path.join __dirname, p

app.use (express.static (pathify "public"))
app.use bodyParser.json() 
app.use bodyParser.urlencoded extended: true
upload = multer { dest: pathify 'uploads' } 

partials.register '.hamlc', (src, opts)->
  hamlc opts.filename, opts, (err, result)->
    throw err if err
    return result

app.engine 'hamlc', hamlc
app.set 'view engine', 'hamlc'
app.set 'layout', 'layout'
app.set "views", (pathify "views")
app.enable "view cache"
app.use partials()

router = require (pathify "routes/router")
router app

port = parseInt process.env.PORT or 3000, 10
app.listen port, ->
	console.log "Okul-Otomasyon #{port} Port'undan Dinlemede!"
