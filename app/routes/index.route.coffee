module.exports = (app) ->
	app.get '/', (req, res) ->
		res.header 'Access-Control-Allow-Origin', "*"
		res.render 'index'
	