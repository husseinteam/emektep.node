module.exports = (app) ->
	app.get '/client/logon', (req, res) ->
		res.header 'Access-Control-Allow-Origin', "*"
		res.status(200).render 'membership/logon', 
			layout: ''
	