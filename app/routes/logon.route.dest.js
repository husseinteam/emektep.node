(function() {
  module.exports = function(app) {
    return app.get('/client/logon', function(req, res) {
      res.header('Access-Control-Allow-Origin', "*");
      return res.status(200).render('membership/logon', {
        layout: ''
      });
    });
  };

}).call(this);
