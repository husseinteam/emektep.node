(function() {
  module.exports = function(app) {
    return app.get('/', function(req, res) {
      res.header('Access-Control-Allow-Origin', "*");
      return res.render('index');
    });
  };

}).call(this);
