module.exports = (app) ->
	app.get '/idare/hocalar', (req, res) ->
		res.header 'Access-Control-Allow-Origin', "*"
		res.status(200).render 'admin/lecturers'
	