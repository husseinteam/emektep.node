(function() {
  module.exports = function(app) {
    return app.get('/idare/hocalar', function(req, res) {
      res.header('Access-Control-Allow-Origin', "*");
      return res.status(200).render('admin/lecturers');
    });
  };

}).call(this);
