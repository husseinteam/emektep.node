glob = require "glob" 
options = 
	nodir: true
	cwd : "#{__dirname}"  
module.exports = (app) ->
	files = glob.sync "**/*.route.dest.js", options
	routes = (require "#{__dirname}/#{file}" for file in files when not file.startsWith("router"))
	for route in routes
		do (route) ->
			route app