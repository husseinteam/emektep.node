(function() {
  var bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; };

  window.Column = (function() {
    function Column(opts) {
      var ref;
      this.hidden = opts.hidden ? ko.observable(true) : ko.observable(false);
      this.ismenu = opts.ismenu ? ko.observable(true) : ko.observable(false);
      this.title = ko.observable(opts.title);
      this.mask = opts.mask;
      this.dataField = ko.observable(opts.dataField);
      this.pattern = ko.observable(opts.pattern);
      this.placeholder = ko.observable(opts.placeholder);
      this.selector = (ref = opts.selector) != null ? ref : function(x) {
        return x["" + opts.dataField];
      };
      this.relatedModel = opts.relatedModel;
      this.objselector = opts.relatedModel ? function(x) {
        return x["" + opts.relatedModel];
      } : null;
      return;
    }

    return Column;

  })();

  window.Cell = (function() {
    function Cell(row, col) {
      this.row = ko.observable(row);
      this.value = ko.computed(function() {
        var inp, ref, val;
        val = col.selector((ref = typeof col.objselector === "function" ? col.objselector(row.obj()) : void 0) != null ? ref : row.obj());
        if (col.mask) {
          inp = $("<input />").val(val);
          val = $(inp).mask(col.mask).val();
        }
        return val;
      });
      this.column = ko.observable(col);
      this.mask = col.mask;
      this.hidden = col.hidden() ? ko.observable(true) : ko.observable(false);
      this.ismenu = col.ismenu() ? ko.observable(true) : ko.observable(false);
    }

    return Cell;

  })();

  window.Row = (function() {
    function Row(obj, columns) {
      this.updater = bind(this.updater, this);
      this.parseObject = bind(this.parseObject, this);
      var col, i, len, ref;
      this.columns = ko.observableArray(columns);
      for (i = 0, len = columns.length; i < len; i++) {
        col = columns[i];
        if (!col.ismenu()) {
          obj.addProperty(col.dataField(), col.selector((ref = typeof col.objselector === "function" ? col.objselector(obj) : void 0) != null ? ref : obj));
        }
      }
      this.obj = ko.observable(obj);
      this.cells = ko.observableArray((function() {
        var j, len1, results;
        results = [];
        for (j = 0, len1 = columns.length; j < len1; j++) {
          col = columns[j];
          results.push(ko.observable(new Cell(this, col)));
        }
        return results;
      }).call(this));
    }

    Row.prototype.parseObject = function(pobj) {
      var col, i, len, ref, ref1;
      ref = this.columns();
      for (i = 0, len = ref.length; i < len; i++) {
        col = ref[i];
        if (!col.ismenu()) {
          pobj.addProperty(col.dataField(), col.selector((ref1 = typeof col.objselector === "function" ? col.objselector(pobj) : void 0) != null ? ref1 : pobj));
        }
      }
      return this.obj(pobj);
    };

    Row.prototype.updater = function(desc) {
      var formModal, formSubmitted, updateForm;
      formModal = $('#form-modal');
      formSubmitted = function(updated) {
        $(formModal).find("form:first").serializeForm(function(sobj) {
          updated(sobj);
        }, function() {
          "Güncelleme Başarısız".talk(desc + " Güncellenemedi. Lütfen İşaretli Alanlara Geçerli Değerler Giriniz", function() {
            failback();
          });
        });
      };
      updateForm = new GenericForm({
        formTitle: "Mevcut " + desc,
        formSubtitle: desc + " Güncelle",
        formSubmitted: formSubmitted,
        node: document.getElementById('form-modal'),
        columns: this.columns()
      });
      $(formModal).on('hidden.bs.modal', (function(_this) {
        return function(e) {
          $(formModal).unbind();
        };
      })(this));
      $(formModal).on('shown.bs.modal', (function(_this) {
        return function() {
          $(formModal).find("form:first").fillForm(_this.obj(), function(arr) {});
        };
      })(this));
      return updateForm;
    };

    return Row;

  })();

  window.GridMaster = (function() {
    function GridMaster(columns, opts) {
      this.enlist = bind(this.enlist, this);
      this.deleteItemClicked = bind(this.deleteItemClicked, this);
      this.editItemClicked = bind(this.editItemClicked, this);
      this.addNewClicked = bind(this.addNewClicked, this);
      this.toModel = bind(this.toModel, this);
      this.inserter = bind(this.inserter, this);
      var idColumn, menuColumn, plusicon, ref;
      menuColumn = new Column({
        hidden: true,
        ismenu: true
      });
      idColumn = new Column({
        hidden: true,
        dataField: 'Id'
      });
      columns.splice(0, 0, menuColumn);
      columns.splice(0, 0, idColumn);
      this.selector = 'grid-master';
      this.columns = ko.observableArray(columns);
      this.rows = ko.observableArray();
      this.description = opts.description;
      ref = opts.routes, this.get = ref.get, this.insert = ref.insert, this.update = ref.update, this.destroy = ref.destroy;
      plusicon = "<i class='fa fa-plus red'></i>";
      this.tableDescription = ko.observable(this.description + " Tablosu");
      this.addNewDescription = ko.observable(this.description + " Ekle " + plusicon);
      this.dataTable = null;
      this.form = ko.observable();
      this.vm = ko.computed((function(_this) {
        return function() {
          return {
            form: _this.form
          };
        };
      })(this));
      ko.applyBindings(this.vm, document.getElementById('form-modal'));
    }

    GridMaster.prototype.inserter = function(desc) {
      var formModal, formSubmitted, insertForm;
      formModal = $('#form-modal');
      formSubmitted = function(inserted) {
        $(formModal).find("form:first").serializeForm(function(sobj) {
          inserted(sobj);
        }, function() {
          "Kayıt Başarısız".talk("Yeni " + desc + " Eklenemedi. Lütfen İşaretli Alanlara Geçerli Değerler Giriniz");
        });
      };
      insertForm = new GenericForm({
        formTitle: "Yeni " + desc,
        formSubtitle: desc + " Ekle",
        formSubmitted: formSubmitted,
        node: document.getElementById('form-modal'),
        columns: this.columns()
      });
      $(formModal).on('hidden.bs.modal', function(e) {
        $(formModal).unbind();
      });
      return insertForm;
    };

    GridMaster.prototype.toModel = function(inserted) {
      var col, i, len, model, ref;
      model = inserted;
      ref = this.columns();
      for (i = 0, len = ref.length; i < len; i++) {
        col = ref[i];
        if (col.relatedModel) {
          model.addProperty(col.relatedModel).addProperty(col.dataField(), model[col.dataField()]);
        }
      }
      return model;
    };

    GridMaster.prototype.addNewClicked = function() {
      this.form(this.inserter(this.description));
      return this.form().showDialog((function(_this) {
        return function(inserted) {
          $('#' + _this.selector).apiMod(_this.insert, 'POST', _this.toModel(inserted)).then(function(result) {
            _this.rows.push(ko.observable(new Row(result, _this.columns())));
            "Kayıt Başarılı".talk("Yeni " + _this.description + " Başarıyla Eklendi", function() {
              return _this.form().closeDialog();
            });
          })["catch"](function(error) {
            console.log(error);
          });
        };
      })(this));
    };

    GridMaster.prototype.editItemClicked = function(cell) {
      this.form(cell.row().updater(this.description));
      return this.form().showDialog((function(_this) {
        return function(inserted) {
          $('#' + _this.selector).apiMod(_this.update + "/" + inserted.Id, 'PUT', _this.toModel(inserted)).then(function(result) {
            cell.row().parseObject(result);
            "Güncelleme Başarılı".talk(_this.description + " Başarıyla Güncellendi", function() {
              return _this.form().closeDialog();
            });
          }, function(error) {
            console.log(error);
          });
        };
      })(this));
    };

    GridMaster.prototype.deleteItemClicked = function(cell, element) {
      "Silme Onayı".ask(this.description + " Silinecektir.", (function(_this) {
        return function(e) {
          return $('#' + _this.selector).apiMod(_this.destroy + "/" + (cell.row().obj().Id), 'DELETE').then(function(result) {
            var ref;
            _this.rows.remove(cell.row());
            if ((ref = _this.dataTable) != null) {
              ref.row($(element).parents('tr')).remove().draw();
            }
            "Kayıt Başarılı".talk(_this.description + " Başarıyla Silindi");
          }, function(error) {
            console.log(error);
          });
        };
      })(this));
    };

    GridMaster.prototype.enlist = function() {
      $('#' + this.selector).apiGet(this.get).then((function(_this) {
        return function(result) {
          var el, rowli;
          rowli = (function() {
            var i, len, ref, results;
            ref = result.CollectionResponse;
            results = [];
            for (i = 0, len = ref.length; i < len; i++) {
              el = ref[i];
              results.push(ko.observable(new Row(el, this.columns())));
            }
            return results;
          }).call(_this);
          _this.rows(rowli);
          ko.applyBindings(_this, document.getElementById(_this.selector + "-container"));
          if (_this.dataTable == null) {
            _this.dataTable = $('#' + _this.selector).toDataTable();
          }
        };
      })(this), (function(_this) {
        return function(error) {
          console.log(error);
        };
      })(this));
    };

    return GridMaster;

  })();

}).call(this);
