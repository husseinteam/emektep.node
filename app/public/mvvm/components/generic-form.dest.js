(function() {
  var bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; };

  window.GenericForm = (function() {
    function GenericForm(opts) {
      this.closeDialog = bind(this.closeDialog, this);
      this.showDialog = bind(this.showDialog, this);
      this.formSubmitted = bind(this.formSubmitted, this);
      var col;
      this.formTitle = ko.observable(opts.formTitle);
      this.formSubtitle = ko.observable(opts.formSubtitle);
      this.columns = ko.observableArray((function() {
        var i, len, ref, results;
        ref = opts.columns;
        results = [];
        for (i = 0, len = ref.length; i < len; i++) {
          col = ref[i];
          if (!col.ismenu()) {
            results.push(col);
          }
        }
        return results;
      })());
      this.node = opts.node;
      this.formSubmittedCallback = opts.formSubmitted;
      this.itemback = null;
    }

    GenericForm.prototype.formSubmitted = function() {
      this.formSubmittedCallback((function(_this) {
        return function(obj) {
          if (typeof _this.itemback === "function") {
            _this.itemback(obj);
          }
        };
      })(this));
    };

    GenericForm.prototype.showDialog = function(itemback) {
      this.itemback = itemback;
      $(this.node).modal('show');
      return this;
    };

    GenericForm.prototype.closeDialog = function() {
      $(this.node).modal('hide');
      return this;
    };

    return GenericForm;

  })();

}).call(this);
