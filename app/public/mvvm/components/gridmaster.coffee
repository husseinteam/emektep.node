class window.Column
	constructor: (opts) ->
		@hidden = if opts.hidden then ko.observable true else ko.observable false
		@ismenu = if opts.ismenu then ko.observable true else ko.observable false
		@title = ko.observable opts.title
		@mask = opts.mask
		@dataField = ko.observable opts.dataField
		@pattern = ko.observable opts.pattern
		@placeholder = ko.observable opts.placeholder
		@selector = opts.selector ? (x)-> x["#{opts.dataField}"]
		@relatedModel = opts.relatedModel
		@objselector = if opts.relatedModel then (x)-> x["#{opts.relatedModel}"] else null
		return

class window.Cell
	constructor: (row, col) ->
		@row = ko.observable row
		@value = ko.computed () ->
			val = col.selector(col.objselector?(row.obj()) ? row.obj())
			if col.mask
				inp = $("<input />").val val
				val = $(inp).mask(col.mask).val()
			val
		@column = ko.observable col
		@mask = col.mask
		@hidden = if col.hidden() then ko.observable true else ko.observable false
		@ismenu = if col.ismenu() then ko.observable true else ko.observable false


class window.Row
	constructor: (obj, columns) ->
		@columns = ko.observableArray columns
		for col in columns
			if not col.ismenu()
				obj.addProperty col.dataField(), (col.selector(col.objselector?(obj) ? obj))
		@obj = ko.observable obj
		@cells = ko.observableArray (ko.observable(new Cell(this, col)) for col in columns)

	parseObject: (pobj) =>
		for col in @columns()
			if not col.ismenu()
				pobj.addProperty col.dataField(), (col.selector(col.objselector?(pobj) ? pobj))
		@obj pobj

	updater: (desc) =>
		formModal = $('#form-modal')
		formSubmitted = (updated) -> 
			$(formModal).find("form:first").serializeForm (sobj) ->
				updated sobj
				return
			, 
			() -> 
				"Güncelleme Başarısız".talk "#{desc} Güncellenemedi. Lütfen İşaretli Alanlara Geçerli Değerler Giriniz", () -> 
					failback()
					return
				return
			return

		updateForm = new GenericForm
			formTitle: "Mevcut #{desc}",
			formSubtitle: "#{desc} Güncelle",
			formSubmitted: formSubmitted,
			node: document.getElementById('form-modal'),
			columns: @columns()

		$(formModal).on 'hidden.bs.modal', (e) =>
			$(formModal).unbind()
			return

		$(formModal).on 'shown.bs.modal', () =>
			$(formModal).find("form:first").fillForm @obj(), (arr) ->
				return
			return	
		updateForm

class window.GridMaster
	constructor: (columns, opts) ->
		menuColumn = new Column
			hidden: true,
			ismenu: true
		idColumn = new Column
			hidden: true,
			dataField: 'Id'
		columns.splice 0, 0, menuColumn
		columns.splice 0, 0, idColumn
		@selector = 'grid-master'
		@columns = ko.observableArray columns
		@rows = ko.observableArray()
		@description = opts.description
		{ @get, @insert, @update, @destroy } = opts.routes
		plusicon = "<i class='fa fa-plus red'></i>"
		@tableDescription = ko.observable("#{@description} Tablosu")
		@addNewDescription = ko.observable("#{@description} Ekle #{plusicon}")
		@dataTable = null
		@form = ko.observable()
		@vm = ko.computed () =>
			form: @form
		ko.applyBindings(@vm, document.getElementById 'form-modal')

	inserter: (desc) =>
		formModal = $('#form-modal')
		formSubmitted = (inserted) -> 
			$(formModal).find("form:first").serializeForm (sobj) ->
				inserted sobj
				return
			, 
			() -> 
				"Kayıt Başarısız".talk "Yeni #{desc} Eklenemedi. Lütfen İşaretli Alanlara Geçerli Değerler Giriniz"
				return
			return

		insertForm = new GenericForm
			formTitle: "Yeni #{desc}",
			formSubtitle: "#{desc} Ekle",
			formSubmitted: formSubmitted,
			node: document.getElementById('form-modal'),
			columns: @columns()

		$(formModal).on 'hidden.bs.modal', (e) ->
			$(formModal).unbind()
			return

		insertForm

	toModel: (inserted) =>
		model = inserted
		for col in @columns()
			if col.relatedModel
				model.addProperty(col.relatedModel).addProperty(col.dataField(), model[col.dataField()])
		model

	addNewClicked: () =>
		@form(@inserter @description)
		@form().showDialog (inserted) =>
			$('#' + @selector).apiMod(@insert, 'POST', @toModel inserted) \
			.then (result) =>
				@rows.push (ko.observable new Row(result, @columns()))
				"Kayıt Başarılı".talk "Yeni #{@description} Başarıyla Eklendi", () => @form().closeDialog()
				return
			.catch (error) ->
				console.log error
				return
			return
	
	editItemClicked: (cell) =>
		@form cell.row().updater @description
		@form().showDialog (inserted) =>
			$('#' + @selector).apiMod("#{@update}/#{inserted.Id}", 'PUT', @toModel inserted)
				.then (result) =>
					cell.row().parseObject result
					"Güncelleme Başarılı".talk "#{@description} Başarıyla Güncellendi", () => @form().closeDialog()
					return
				,
				(error) ->
					console.log error
					return
			return
	
	deleteItemClicked: (cell, element) =>
		"Silme Onayı".ask "#{@description} Silinecektir.", (e) =>
			$('#' + @selector).apiMod("#{@destroy}/#{cell.row().obj().Id}", 'DELETE')
				.then (result) =>
					@rows.remove cell.row()
					@dataTable?.row($(element).parents('tr')).remove().draw();
					"Kayıt Başarılı".talk "#{@description} Başarıyla Silindi"
					return
				,
				(error) ->
					console.log error
					return
		return

	enlist: () =>
		$('#' + @selector).apiGet(@get)
			.then (result) =>
				rowli = (ko.observable(new Row(el, @columns())) for el in result.CollectionResponse)
				@rows rowli
				ko.applyBindings this, (document.getElementById "#{@selector}-container")
				@dataTable ?= $('#' + @selector).toDataTable()
				return
			,
			(error) =>
				console.log error
				return
		return


