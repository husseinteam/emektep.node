class window.GenericForm
	constructor: (opts) ->
		@formTitle = ko.observable opts.formTitle
		@formSubtitle = ko.observable opts.formSubtitle
		@columns = ko.observableArray (col for col in opts.columns when not col.ismenu())
		@node = opts.node
		@formSubmittedCallback = opts.formSubmitted
		@itemback = null

	formSubmitted: () =>
		@formSubmittedCallback (obj) =>
			@itemback? obj
			return
		return

	showDialog: (itemback) => 
		@itemback = itemback
		$(@node).modal('show')
		return this

	closeDialog: () => 
		$(@node).modal('hide')
		return this
