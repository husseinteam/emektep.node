
columns = [
	new Column
		title: 'E-Posta',
		dataField: 'Email',
		placeholder: 'Lütfen E-Posta Giriniz',
		pattern: /\w+\s\w+(\s+\w+)?/g,
	new Column
		title: 'Ad',
		placeholder: 'Lütfen Adınızı Giriniz',
		dataField: 'FirstName',
		pattern: /^\w+$/g,
	new Column
		title: 'Soyad',
		placeholder: 'Lütfen Soyadınızı Giriniz',
		dataField: 'LastName',
		pattern: /^\w+$/g,
	new Column
		title: 'Telefon Numarası',
		placeholder: 'Lütfen Telefon Numaranızı Giriniz',
		dataField: 'PhoneAlternate',
		pattern: /^\w+$/g,
		relatedModel: 'UserProfile',
		mask: '(000) 000-0000'
	new Column
		title: 'Mevkii',
		placeholder: 'Lütfen Mevkiinizi Giriniz',
		dataField: 'Position',
		pattern: /^\w+$/g,
		relatedModel: 'UserProfile',
	new Column
		hidden: true,
		ismenu: false,
		relatedModel: 'UserProfile',
		dataField: 'ID'
]

window.Web.registerReady () ->
	window.gridmaster = new GridMaster columns, 
		description: 'Öğretmen',
		routes: 
			get: 'users/list/admin'
			insert: 'users/add',
			update: 'users/update',
			destroy: 'users/delete'
	window.gridmaster.enlist()
	return
