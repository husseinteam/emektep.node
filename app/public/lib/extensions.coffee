window.routeToUrl = (route) ->
	#"http://192.168.1.200:90/#{route}"
	"http://app.lampiclobe.com/#{route}"
window.getToken = (tokencb) ->
	#if not Cookies.get("access_token")
	if true
		$.ajax(
			method: 'POST',
			url: (window.routeToUrl "token"),
			crossDomain: true,
			dataType: 'json',
			data: jQuery.param(
				'grant_type': 'password',
				'UserName': Cookies.get("UserName") ? "ozgurs@okul.com",
				'Password': Cookies.get("Password") ? "P2ssw0rd!")
		)
		.done((data) ->
			Cookies.set "access_token", data.access_token
			tokencb data
			return
		)
		.fail((xhr, textStatus, errorThrown) -> 
			"Güvenlik İhlali".talk "Sistem Hatası. Lütfen Yeniden Deneyiniz", () -> 
				console.log textStatus
				$.unblockUI()
				return
			return
		)
	else
		tokencb access_token: Cookies.get("access_token")

class window.Web 
	constructor: () ->
		return
	@readyCallbacks: []
	@registerReady: (callback) =>
		Web.readyCallbacks.push callback
		return
	@runall: () =>
		(cb() for cb in Web.readyCallbacks)
		return

$ ->
	Web.runall()
	return

jQuery.fn.extend
	hasattr: (attrname) ->
		attr = $(this).attr(attrname)
		typeof attr isnt typeof undefined and attr isnt false
	,
	apiMod: (route, method, data) ->
		promise = new Promise((resolve, reject) ->
			$.blockUI message: '<img src="/img/busy.gif" /> Yükleniyor...'
			window.getToken (tokenresp) -> 
				if not tokenresp.error 
					$.ajax(
						method: method,
						url: (window.routeToUrl "api/#{route}"),
						data: data ? {},
						crossDomain: true,
						dataType: 'json',
						headers: 
							if method is 'DELETE'
								'Authorization': "Bearer #{Cookies.get('access_token')}",
								"Content-Type": "application/json",
								"Accept": "application/json"
							else
								'Authorization': "Bearer #{Cookies.get('access_token')}")
					.done((d) ->
						if d.Success
							resolve d.Data
						else
							reject d.Fault
					)
					.fail((xhr, textStatus, errorThrown) -> 
						reject
							status: textStatus,
							error: errorThrown
					)
					.always(() ->
						$.unblockUI()
					)
				else
					"Hatalı İşlem".talk error_description
				return
			return
		)
		promise
	,
	apiGet: (route) ->
		promise = new Promise((resolve, reject) ->
			$.blockUI message: '<img src="/img/busy.gif" /> Yükleniyor...'
			window.getToken (tokenresp) -> 
				if not tokenresp.error 
					$.ajax(
						method: 'GET',
						url: (window.routeToUrl "api/#{route}"),
						crossDomain: true,
						dataType: 'json',
						headers: 
							'Authorization': "Bearer #{Cookies.get('access_token')}"
					)
					.done((data) ->
						resolve(data)
					)
					.fail((xhr, textStatus, errorThrown) -> 
						reject
							status: textStatus,
							error: errorThrown
					)
					.always(() ->
						$.unblockUI()
					)
				else
					"Hatalı İşlem".talk error_description
				return
			return
		)
		promise
	, 
	toDataTable: () ->
		oTable = $(this).DataTable
			"language":
				"sDecimal":        ",",
				"sEmptyTable":     "Tabloda herhangi bir veri mevcut değil",
				"sInfo":           "_TOTAL_ kayıttan _START_ - _END_ arasındaki kayıtlar gösteriliyor",
				"sInfoEmpty":      "",
				"sInfoFiltered":   "(_MAX_ kayıt içerisinden bulunan)",
				"sInfoPostFix":    "",
				"sInfoThousands":  ".",
				"sLengthMenu":     "Sayfada _MENU_ kayıt göster",
				"sLoadingRecords": "Yükleniyor...",
				"sProcessing":     "İşleniyor...",
				"sSearch":         "Ara:",
				"sZeroRecords":    "Eşleşen kayıt bulunamadı",
				"oPaginate": 
					"sFirst":    "İlk",
					"sLast":     "Son",
					"sNext":     "Sonraki",
					"sPrevious": "Önceki"
				,
				"oAria": 
					"sSortAscending":  ": artan sütun sıralamasını aktifleştir",
					"sSortDescending": ": azalan sütun soralamasını aktifleştir"
				
			,
			buttons: [
				{ 
					extend: 'print',
					customize: (win) ->
						$(win.document.body).css backgroundColor: '#FFF', opacity: '0.9'
						info = oTable.page.info()
						delta = oTable.iDisplayLength
						top = delta * 0.4
						$(win.document.body).prepend "<img style='position:absolute; opacity: 0.3; width: 50%; z-index: 99; top:29%; left:23%;' src='#{window.location.origin}/img/logo.png'/>"
						$(win.document.body).find('table')
							.removeClass('table-striped')
							.addClass('compact')
					, 
					className: 'btn dark btn-outline',
					exportOptions: columns:  ':not(:nth-child(1))'
				},
				{ extend: 'copy', className: 'btn red btn-outline', exportOptions: columns:  ':not(:nth-child(1))' },
				{ extend: 'pdf', className: 'btn green btn-outline', exportOptions: columns:  ':not(:nth-child(1))' },
				{ extend: 'excel', className: 'btn yellow btn-outline', exportOptions: columns:  ':not(:nth-child(1))' },
				{ extend: 'csv', className: 'btn purple btn-outline', exportOptions: columns:  ':not(:nth-child(1))' }
			],

			responsive: true,

			"order": [
				[1, 'desc']
			],
			"lengthMenu": [
				[5, 10, 15, 20, -1],
				[5, 10, 15, 20, "Hepsi"]
			],

			"pageLength": 10

		$(this).closest('.portlet').find('li > a.tool-action').on 'click', () ->
			action = $(this).attr 'data-action'
			oTable.button(action).trigger()
	   
		oTable
	,
	serializeForm: (callback, failback) ->
		checked = true;
		o = {}
		arr = this.serializeArray()
		for a, i in arr
			el = $('#' + a.name)
			if $(el).length is 0
				el = $("select[name='#{a.name}']")
				if $(el).length is 0
					el = $("textarea[name='#{a.name}']")
					if $(el).length is 0
						el = $("input[name='#{a.name}']")
					if $(el).length is 0
						el = $("input[name='#{a.name}'][value='#{a.value}']")

			val = if $(el).is 'radio' then $("input[name='#{a.name}']:checked").val() else $(el).val()
			o.addProperty a.name, val
			if not $(el).is(":hidden") and not $(el).hasattr 'data-nullable'
				if $(el).val()
					$(el).parent('div').removeClass('has-success').removeClass('has-error').addClass('has-success')
				else
					$(el).parent('div').removeClass('has-success').removeClass('has-error').addClass('has-error')
					checked = false

			if i is arr.length - 1
				if checked
					callback o 
				else 
					failback?()
	,
	fillForm: (model, callback) ->
		arr = this.serializeArray()
		len = arr.length
		for a, index in arr
			el = $('#' + a.name)
			if $(el).length is 0
				el = $("select[name='#{a.name}']")
				if $(el).length is 0
					el = $("textarea[name='#{a.name}']")
					if $(el).length is 0
						el = $("input[name='#{a.name}']")

			if $(el).attr('type') is 'radio'
				$("input[name='#{a.name}'][value='#{model[a.name]}']").attr 'checked', true
			else
				$(el).val model[a.name]
			if index is len - 1
				callback arr

	







