(function() {
  Object.defineProperty(String.prototype, "onlyAssignee", {
    value: function(handler) {
      var i, key, len, obs;
      obs = this;
      for (i = 0, len = obs.length; i < len; i++) {
        key = obs[i];
        if (obs.hasOwnProperty(key)) {
          if (obs[key] == null) {
            delete obs[key];
          }
        }
      }
      return obs;
    },
    enumerable: false
  });

  Object.defineProperty(Object.prototype, "clone", {
    value: function() {
      var i, key, len, obj;
      obj = {};
      for (i = 0, len = this.length; i < len; i++) {
        key = this[i];
        if (this.hasOwnProperty(key)) {
          Object.defineProperty(obj, key, {
            value: this[key],
            enumerable: false
          });
        }
      }
      return obj;
    },
    enumerable: false
  });

  Object.defineProperty(Object.prototype, "addProperty", {
    value: function(prop, val) {
      if (!this.hasOwnProperty(prop)) {
        Object.defineProperty(this, prop, {
          value: val === false ? false : val != null ? val : {},
          configurable: true,
          writable: true,
          enumerable: true
        });
      }
      return this[prop];
    },
    enumerable: false
  });

  Object.defineProperty(Object.prototype, "project", {
    value: function(ko, obj) {
      var i, len, prop;
      for (i = 0, len = this.length; i < len; i++) {
        prop = this[i];
        if (this.hasOwnProperty(prop)) {
          if (typeof this[prop] === 'object') {
            this[prop].project(ko, obj[prop]);
          }
          if (ko.isObservable(this[prop])) {
            this[prop](obj[prop]);
          } else {
            this[prop] = obj[prop];
          }
        }
      }
      return this;
    },
    enumerable: false
  });

  Object.defineProperty(String.prototype, "register", {
    value: function(handler) {
      var callback, eventName, handle, target;
      eventName = this;
      target = document.body;
      if (target.addEventListener) {
        handle = function(event) {
          handler(event);
          return target.removeEventListener(eventName, handle, false);
        };
        target.addEventListener(eventName, handle, false);
      } else {
        callback = function() {
          handler.call(document);
          return target.detachEvent('on' + eventName, callback);
        };
        target.attachEvent('on' + eventName, callback);
      }
    },
    enumerable: false
  });

  Object.defineProperty(String.prototype, "dispatch", {
    value: function(data) {
      var event, eventName, target;
      eventName = this;
      target = document.body;
      event = null;
      if (document.createEvent) {
        event = document.createEvent("HTMLEvents");
        event.initEvent(eventName, true, true);
      } else {
        event = document.createEventObject();
        event.eventType = eventName;
      }
      event.eventName = eventName;
      event.data = data;
      if (document.createEvent) {
        target.dispatchEvent(event);
      } else {
        target.fireEvent("on" + event.eventType, event);
      }
    },
    enumerable: false
  });

  Object.defineProperty(Object.prototype, "talk", {
    value: function(text, hiddenback, timeout) {
      var oldZIndex, speaker;
      speaker = $('#spoken-modal');
      oldZIndex = $(".modal").length > 0 ? parseInt($(".modal:first").css('z-index').replace('px', ''), 10) : -1;
      $(speaker).find('.modal-header:first').html(this.toString());
      $(speaker).on('hidden.bs.modal', function(event) {
        $(speaker).unbind();
        return typeof hiddenback === "function" ? hiddenback() : void 0;
      });
      $(speaker).find('.modal-body:first').html("<p class='text-center'>" + text + "</p>");
      $(speaker).css('z-index', oldZIndex + 500);
      $(speaker).find("#modal-ok").hide();
      $(speaker).find("#modal-close").text("Kapat");
      $(speaker).modal();
      if (timeout) {
        return setTimeout(function() {
          return $(speaker).find('[data-dismiss="modal"]:first').click();
        }, timeout);
      }
    },
    enumerable: false
  });

  Object.defineProperty(Object.prototype, "ask", {
    value: function(text, okback, cancelback) {
      var oldZIndex, speaker;
      speaker = $('#spoken-modal');
      oldZIndex = $(".modal").length > 0 ? parseInt($(".modal:first").css('z-index').replace('px', ''), 10) : -1;
      $(speaker).find('.modal-header:first').html(this.toString());
      $(speaker).on('hidden.bs.modal', function(event) {
        return $(speaker).unbind();
      });
      $(speaker).find('.modal-body:first').html("<p class='text-center'>" + text + "</p>");
      $(speaker).css('z-index', oldZIndex + 500);
      $(speaker).find("#modal-ok").show().text("Tamam").click(okback);
      $(speaker).find("#modal-close").text("İptal").click(cancelback);
      return $(speaker).modal();
    },
    enumerable: false
  });

}).call(this);
