(function() {
  window.routeToUrl = function(route) {
    return "http://app.lampiclobe.com/" + route;
  };

  window.getToken = function(tokencb) {
    var ref, ref1;
    if (true) {
      return $.ajax({
        method: 'POST',
        url: window.routeToUrl("token"),
        crossDomain: true,
        dataType: 'json',
        data: jQuery.param({
          'grant_type': 'password',
          'UserName': (ref = Cookies.get("UserName")) != null ? ref : "ozgurs@okul.com",
          'Password': (ref1 = Cookies.get("Password")) != null ? ref1 : "P2ssw0rd!"
        })
      }).done(function(data) {
        Cookies.set("access_token", data.access_token);
        tokencb(data);
      }).fail(function(xhr, textStatus, errorThrown) {
        "Güvenlik İhlali".talk("Sistem Hatası. Lütfen Yeniden Deneyiniz", function() {
          console.log(textStatus);
          $.unblockUI();
        });
      });
    } else {
      return tokencb({
        access_token: Cookies.get("access_token")
      });
    }
  };

  window.Web = (function() {
    function Web() {
      return;
    }

    Web.readyCallbacks = [];

    Web.registerReady = function(callback) {
      Web.readyCallbacks.push(callback);
    };

    Web.runall = function() {
      var cb, j, len1, ref;
      ref = Web.readyCallbacks;
      for (j = 0, len1 = ref.length; j < len1; j++) {
        cb = ref[j];
        cb();
      }
    };

    return Web;

  })();

  $(function() {
    Web.runall();
  });

  jQuery.fn.extend({
    hasattr: function(attrname) {
      var attr;
      attr = $(this).attr(attrname);
      return typeof attr !== typeof void 0 && attr !== false;
    },
    apiMod: function(route, method, data) {
      var promise;
      promise = new Promise(function(resolve, reject) {
        $.blockUI({
          message: '<img src="/img/busy.gif" /> Yükleniyor...'
        });
        window.getToken(function(tokenresp) {
          if (!tokenresp.error) {
            $.ajax({
              method: method,
              url: window.routeToUrl("api/" + route),
              data: data != null ? data : {},
              crossDomain: true,
              dataType: 'json',
              headers: method === 'DELETE' ? {
                'Authorization': "Bearer " + (Cookies.get('access_token')),
                "Content-Type": "application/json",
                "Accept": "application/json"
              } : {
                'Authorization': "Bearer " + (Cookies.get('access_token'))
              }
            }).done(function(d) {
              if (d.Success) {
                return resolve(d.Data);
              } else {
                return reject(d.Fault);
              }
            }).fail(function(xhr, textStatus, errorThrown) {
              return reject({
                status: textStatus,
                error: errorThrown
              });
            }).always(function() {
              return $.unblockUI();
            });
          } else {
            "Hatalı İşlem".talk(error_description);
          }
        });
      });
      return promise;
    },
    apiGet: function(route) {
      var promise;
      promise = new Promise(function(resolve, reject) {
        $.blockUI({
          message: '<img src="/img/busy.gif" /> Yükleniyor...'
        });
        window.getToken(function(tokenresp) {
          if (!tokenresp.error) {
            $.ajax({
              method: 'GET',
              url: window.routeToUrl("api/" + route),
              crossDomain: true,
              dataType: 'json',
              headers: {
                'Authorization': "Bearer " + (Cookies.get('access_token'))
              }
            }).done(function(data) {
              return resolve(data);
            }).fail(function(xhr, textStatus, errorThrown) {
              return reject({
                status: textStatus,
                error: errorThrown
              });
            }).always(function() {
              return $.unblockUI();
            });
          } else {
            "Hatalı İşlem".talk(error_description);
          }
        });
      });
      return promise;
    },
    toDataTable: function() {
      var oTable;
      oTable = $(this).DataTable({
        "language": {
          "sDecimal": ",",
          "sEmptyTable": "Tabloda herhangi bir veri mevcut değil",
          "sInfo": "_TOTAL_ kayıttan _START_ - _END_ arasındaki kayıtlar gösteriliyor",
          "sInfoEmpty": "",
          "sInfoFiltered": "(_MAX_ kayıt içerisinden bulunan)",
          "sInfoPostFix": "",
          "sInfoThousands": ".",
          "sLengthMenu": "Sayfada _MENU_ kayıt göster",
          "sLoadingRecords": "Yükleniyor...",
          "sProcessing": "İşleniyor...",
          "sSearch": "Ara:",
          "sZeroRecords": "Eşleşen kayıt bulunamadı",
          "oPaginate": {
            "sFirst": "İlk",
            "sLast": "Son",
            "sNext": "Sonraki",
            "sPrevious": "Önceki"
          },
          "oAria": {
            "sSortAscending": ": artan sütun sıralamasını aktifleştir",
            "sSortDescending": ": azalan sütun soralamasını aktifleştir"
          }
        },
        buttons: [
          {
            extend: 'print',
            customize: function(win) {
              var delta, info, top;
              $(win.document.body).css({
                backgroundColor: '#FFF',
                opacity: '0.9'
              });
              info = oTable.page.info();
              delta = oTable.iDisplayLength;
              top = delta * 0.4;
              $(win.document.body).prepend("<img style='position:absolute; opacity: 0.3; width: 50%; z-index: 99; top:29%; left:23%;' src='" + window.location.origin + "/img/logo.png'/>");
              return $(win.document.body).find('table').removeClass('table-striped').addClass('compact');
            },
            className: 'btn dark btn-outline',
            exportOptions: {
              columns: ':not(:nth-child(1))'
            }
          }, {
            extend: 'copy',
            className: 'btn red btn-outline',
            exportOptions: {
              columns: ':not(:nth-child(1))'
            }
          }, {
            extend: 'pdf',
            className: 'btn green btn-outline',
            exportOptions: {
              columns: ':not(:nth-child(1))'
            }
          }, {
            extend: 'excel',
            className: 'btn yellow btn-outline',
            exportOptions: {
              columns: ':not(:nth-child(1))'
            }
          }, {
            extend: 'csv',
            className: 'btn purple btn-outline',
            exportOptions: {
              columns: ':not(:nth-child(1))'
            }
          }
        ],
        responsive: true,
        "order": [[1, 'desc']],
        "lengthMenu": [[5, 10, 15, 20, -1], [5, 10, 15, 20, "Hepsi"]],
        "pageLength": 10
      });
      $(this).closest('.portlet').find('li > a.tool-action').on('click', function() {
        var action;
        action = $(this).attr('data-action');
        return oTable.button(action).trigger();
      });
      return oTable;
    },
    serializeForm: function(callback, failback) {
      var a, arr, checked, el, i, j, len1, o, results, val;
      checked = true;
      o = {};
      arr = this.serializeArray();
      results = [];
      for (i = j = 0, len1 = arr.length; j < len1; i = ++j) {
        a = arr[i];
        el = $('#' + a.name);
        if ($(el).length === 0) {
          el = $("select[name='" + a.name + "']");
          if ($(el).length === 0) {
            el = $("textarea[name='" + a.name + "']");
            if ($(el).length === 0) {
              el = $("input[name='" + a.name + "']");
            }
            if ($(el).length === 0) {
              el = $("input[name='" + a.name + "'][value='" + a.value + "']");
            }
          }
        }
        val = $(el).is('radio') ? $("input[name='" + a.name + "']:checked").val() : $(el).val();
        o.addProperty(a.name, val);
        if (!$(el).is(":hidden") && !$(el).hasattr('data-nullable')) {
          if ($(el).val()) {
            $(el).parent('div').removeClass('has-success').removeClass('has-error').addClass('has-success');
          } else {
            $(el).parent('div').removeClass('has-success').removeClass('has-error').addClass('has-error');
            checked = false;
          }
        }
        if (i === arr.length - 1) {
          if (checked) {
            results.push(callback(o));
          } else {
            results.push(typeof failback === "function" ? failback() : void 0);
          }
        } else {
          results.push(void 0);
        }
      }
      return results;
    },
    fillForm: function(model, callback) {
      var a, arr, el, index, j, len, len1, results;
      arr = this.serializeArray();
      len = arr.length;
      results = [];
      for (index = j = 0, len1 = arr.length; j < len1; index = ++j) {
        a = arr[index];
        el = $('#' + a.name);
        if ($(el).length === 0) {
          el = $("select[name='" + a.name + "']");
          if ($(el).length === 0) {
            el = $("textarea[name='" + a.name + "']");
            if ($(el).length === 0) {
              el = $("input[name='" + a.name + "']");
            }
          }
        }
        if ($(el).attr('type') === 'radio') {
          $("input[name='" + a.name + "'][value='" + model[a.name] + "']").attr('checked', true);
        } else {
          $(el).val(model[a.name]);
        }
        if (index === len - 1) {
          results.push(callback(arr));
        } else {
          results.push(void 0);
        }
      }
      return results;
    }
  });

}).call(this);
