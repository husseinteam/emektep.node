Object.defineProperty String.prototype, "onlyAssignee",
	value: (handler) -> 
		obs = this; 
		for key in obs
			if obs.hasOwnProperty(key)
				if not obs[key]?
					delete obs[key]
		obs
	,
	enumerable: false

Object.defineProperty Object.prototype, "clone",
	value: () -> 
		obj = {}; 
		for key in this
			if this.hasOwnProperty key
				Object.defineProperty obj, key,
					value: this[key],
					enumerable: false
		obj
	,
	enumerable: false

Object.defineProperty Object.prototype, "addProperty",
	value: (prop, val) -> 
		if not this.hasOwnProperty prop
			Object.defineProperty this, prop,
				value: if val == false then false else val ? {},
				configurable: true,
				writable: true,
				enumerable: true
		this[prop]
	,
	enumerable: false

Object.defineProperty Object.prototype, "project",
	value: (ko, obj) -> 
		for prop in this
			if this.hasOwnProperty prop
				if typeof this[prop] is 'object' 
					this[prop].project(ko, obj[prop])
				if ko.isObservable this[prop]
					this[prop](obj[prop])
				else
					this[prop] = obj[prop]
		return this
	,
	enumerable: false

Object.defineProperty String.prototype, "register",
	value: (handler) -> 
		eventName = this
		target = document.body
		if target.addEventListener
			handle = (event) ->
				handler(event)
				target.removeEventListener eventName, handle, false
			target.addEventListener eventName, handle, false
		else
			callback = () ->
				handler.call(document)
				target.detachEvent 'on' + eventName, callback
			target.attachEvent 'on' + eventName, callback
		return
	,
	enumerable: false

Object.defineProperty String.prototype, "dispatch",
	value: (data) -> 
		eventName = this
		target = document.body
		event = null
		if document.createEvent
			event = document.createEvent "HTMLEvents"
			event.initEvent eventName, true, true
		else 
			event = document.createEventObject()
			event.eventType = eventName

		event.eventName = eventName
		event.data = data
		if document.createEvent
			target.dispatchEvent event
		else 
			target.fireEvent "on#{event.eventType}", event
		return
	,
	enumerable: false

Object.defineProperty Object.prototype, "talk", 
	value: (text, hiddenback, timeout) ->
		speaker = $('#spoken-modal')
		oldZIndex = if $(".modal").length > 0 then parseInt($(".modal:first").css('z-index').replace('px', ''), 10) else -1
		$(speaker).find('.modal-header:first').html this.toString()
		$(speaker).on 'hidden.bs.modal', (event) ->
			$(speaker).unbind()
			hiddenback?()
		$(speaker).find('.modal-body:first').html("<p class='text-center'>#{text}</p>")
		$(speaker).css 'z-index', oldZIndex + 500

		$(speaker).find("#modal-ok").hide();
		$(speaker).find("#modal-close").text("Kapat");

		$(speaker).modal()
		if timeout
			setTimeout () ->
				$(speaker).find('[data-dismiss="modal"]:first').click()
			, timeout
	,
	enumerable: false

Object.defineProperty Object.prototype, "ask", 
	value: (text, okback, cancelback) ->
		speaker = $('#spoken-modal')
		oldZIndex = if $(".modal").length > 0 then parseInt($(".modal:first").css('z-index').replace('px', ''), 10) else -1
		$(speaker).find('.modal-header:first').html this.toString()

		$(speaker).on 'hidden.bs.modal', (event) ->
			$(speaker).unbind()

		$(speaker).find('.modal-body:first').html("<p class='text-center'>#{text}</p>")
		$(speaker).css 'z-index', oldZIndex + 500

		$(speaker).find("#modal-ok").show().text("Tamam").click(okback);
		$(speaker).find("#modal-close").text("İptal").click(cancelback);

		$(speaker).modal()

	,
	enumerable: false



